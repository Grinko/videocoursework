package grinko;

import com.orsoncharts.Chart3D;
import com.orsoncharts.Chart3DFactory;
import com.orsoncharts.Chart3DPanel;
import com.orsoncharts.data.xyz.XYZSeries;
import com.orsoncharts.data.xyz.XYZSeriesCollection;
import com.orsoncharts.plot.XYZPlot;
import com.orsoncharts.renderer.xyz.ScatterXYZRenderer;
import org.bytedeco.javacpp.opencv_core;
import org.bytedeco.javacv.CanvasFrame;
import org.bytedeco.javacv.FrameGrabber;
import org.bytedeco.javacv.OpenCVFrameGrabber;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.VectorRenderer;
import org.jfree.data.xy.VectorSeries;
import org.jfree.data.xy.VectorSeriesCollection;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import system.*;
import system.Frame;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.List;

/**
 * Author Grinch
 * Date: 07.10.14
 * Time: 22:36
 */
public class Controller {
    //   public static String fileName = "cars.mp4";
    public static String fileName = "cars2.3gp";
    //   public static String fileName = "cars3.3gp";
    //  public static String fileName = "cars4.webm";

    Frame model;
    private static Controller ourInstance;
    Correlator correlator = null;

    CanvasFrame canvas;

    public static Controller getInstance() {
        if (ourInstance == null) ourInstance = new Controller();
        return ourInstance;
    }

    private Controller() {
        loadVideo();
    }

    public static void main(String[] args){
        Controller c = Controller.getInstance();
    }

    public void loadVideo(){
        canvas = new CanvasFrame("Видео");

        canvas.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);


        byte[][][] video = null;// [цвет] [ высота] [ ширина]
        int[] size = new int[4];
        size[0] = 3;
        size[1] = 1;
        int frameCount = 0;

        FrameGrabber grabber = new OpenCVFrameGrabber(fileName);
        try {
            grabber.start();           // VORUEM KARAVAN
            opencv_core.IplImage img;
            try{
                while (true) {
                    img = grabber.grab();
                    BufferedImage bi = img.getBufferedImage();
                    if (video == null) {
                        size[2] = bi.getHeight();
                        size[3] = bi.getWidth();
                        video = new byte[size[0]][size[2]][size[3]];
                    }
                    int[] color = bi.getRGB(0, 0, size[3], size[2], null, 0, size[3]);
                    for (int h = 0; h < size[2]; h++) {
                        for (int w = 0; w < size[3]; w++) {
                            int c = color[w + h * size[3]];
                            video[0][h][w] = (byte) ((c >> 16) & 0xff);
                            video[1][h][w] = (byte) ((c >> 8) & 0xff);
                            video[2][h][w] = (byte) (c & 0xff);
                        }
                    }
                    if (model == null) {
                        model = new system.Frame(video);
                    }

                    //НикиткинСценарий(frameCount);
                    АртемкинСценарий(frameCount);

                    frameCount++;

                    canvas.showImage(model.getSource());
                    model.reset();
                }


            }catch (FrameGrabber.Exception ex){

            }

            System.out.println("Чтение видео-файла закончено.");

        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
        System.out.println("Конец!");
    }

    JFrame chartFrame = new JFrame("Гистограмма");
    JFrame kFrame = new JFrame("Корреляция");
    JFrame vpFrame = new JFrame("Оптический поток");

    void НикиткинСценарий(int frameCount){
        if (frameCount > 2) {
            //    model.noise();
        }
        if (frameCount > 3) {
            //  model.RGBToYCbCr();
        }
        if (frameCount > 5) {
            // model.YCbCrToGrayScale();
        }
        if (frameCount > 6) {
            //model.RGBtoGrayScale();
        }
        if (frameCount > 9) {
            //model.gaussianFilter();
        }
        if (frameCount > 12) {
            double[] d = model.otcuBin();
            drawHist(model,d,chartFrame);
           /* if (correlator == null){
                correlator = new Correlator(model.getEtalons(3,3));
                EtalonImage eimg = correlator.getEtalon(4);
                etalonCanvas.setSize(eimg.getW(),eimg.getH());
                etalonCanvas.showImage(eimg.getSource());
                java.util.List<double[]> K = correlator.findNormal(model,4);
                drawK(K,kFrame,correlator.getResult());
            }*//* else {
                            for (int i = 0 ; i < 1; i++) {
                                etalonCanvas.showImage(correlator.getEtalon(i).getSource());
                                Thread.sleep(500);
                            }
                        }*//*

            */

        }
        if (frameCount > 13 ) {
            int count = 20;
            int count2 = 20;
            int pad = 10;
            int fCount = 1;
            if (correlator == null){

                correlator = new Correlator(model.getEtalons(count,count2));
                //EtalonImage eimg = correlator.getEtalon(4);
                //etalonCanvas.setSize(eimg.getW(),eimg.getH());
                //etalonCanvas.showImage(eimg.getSource());
                //java.util.List<double[]> K = correlator.find(model,4,50);
                //drawK(K,kFrame,correlator.getResult());
            }

            if (correlator.getEtalon(0).getX().size() > fCount ){
                drawVectorPlot(correlator,vpFrame,model.getH());
                correlator = new Correlator(model.getEtalons(count,count2));
            }else{
                correlator.findField(model,pad);
            }
        }
    }

    void АртемкинСценарий(int frameCount){
        if (frameCount > 1) {
            model.RGBToYCbCr();
        }
        if (frameCount > 4) {
            model.YCbCrToGrayScale();
        }
        if (frameCount > 8) {
            //    model.noise();
        }

        if (frameCount > 11) {
            model.gaussianFilter();
        }
        if (frameCount > 12) {
            model.YCbCrBin((235-16)/2.0);

        }

        if (frameCount > 13 ) {
            int count = 30;           //кол-во эталонов по высоте
            int count2 = 40;          //кол-во эталонов по длине
            int pad = 10;              //размер области поиска для эталонов
            int fCount = 1;           //количество кадров для формирования вектора
            int recoursePad = 2;      //как далеко рекурсия ищет соседние вектора
            int minimumEtalons = 4;   //какое минимально кол-во векторов является областью
            int maximumEtalons = 20;   //какое максимальное кол-во векторов является областью
            int pathRadius     = 60;   //на каком расстоянии точки можно связать в путь
            if (correlator == null){

                correlator = new Correlator(model.getEtalons(count,count2));     // Создаем корелятор
                //EtalonImage eimg = correlator.getEtalon(4);
                //etalonCanvas.setSize(eimg.getW(),eimg.getH());
                //etalonCanvas.showImage(eimg.getSource());
                //java.util.List<double[]> K = correlator.find(model,4,50);
                //drawK(K,kFrame,correlator.getResult());
            }

            if (correlator.getEtalon(0).getX().size() > fCount ){
                model.doConnectedSpaces(correlator,recoursePad,minimumEtalons,maximumEtalons);   // Нвходмс связные облаасти ОП
                model.doCars(pathRadius);
                drawVectorPlot(correlator,vpFrame,model.getH());
                correlator = new Correlator(model.getEtalons(count,count2));
            }else{
                correlator.findField(model,pad);                                 // Строим оптический поток
            }
        }
    }

    void drawVectorPlot(Correlator correlator,JFrame frame,int h){
        VectorSeriesCollection collection = new VectorSeriesCollection();
        VectorSeries series = new VectorSeries("Поле");
        for (int i = 0; i < correlator.getEtalonCount(); i++){

            EtalonImage eimg = correlator.getEtalon(i);
            List<Integer> x = eimg.getX();
            List<Integer> y = eimg.getY();

            int x0 = x.get(0);
            int y0 = h- y.get(0);
            int x1 = x.get(x.size()-1);
            int y1 = h-y.get(y.size()-1);
            series.add(x0, y0, (x1 - x0), (y1 - y0));
        }
        collection.addSeries(series);
        VectorRenderer vectorrenderer = new VectorRenderer();
        vectorrenderer.setSeriesPaint(0, Color.blue);

        XYPlot plot = new XYPlot(collection,new NumberAxis("x"),new NumberAxis("y"),vectorrenderer);
        JFreeChart chart = new JFreeChart(
                "Оптический поток",
                new Font("a",4,14),
                plot,
                false);


        ChartPanel panel = new ChartPanel(chart);
        frame.setSize(640, 480);
        frame.setContentPane(panel);
        frame.setVisible(true);

    }

    void drawK(List<double[]> K,JFrame frame,double[] result){
        XYZSeries series = new XYZSeries("K(x,y)");
        for (int i = 0; i < K.size(); i++) {
            double[] data = K.get(i);
            series.add(data[0],data[1],data[2]);
        }
        XYZSeriesCollection collection = new XYZSeriesCollection();
        collection.add(series);

        Chart3D chart = Chart3DFactory.createScatterChart(
                "Корреляция",
                "max K  x = " + String.valueOf(result[0])+ " y =" + String.valueOf(result[1]) + "\n K = " +String.valueOf(result[2]) ,
                collection,
                "x",
                "y",
                "K"
        );


        XYZPlot plot3 = (XYZPlot) chart.getPlot();
        ScatterXYZRenderer renderer = (ScatterXYZRenderer) plot3.getRenderer();
        renderer.setSize(0.1);
        renderer.setColors(new Color(255,0,0),new Color(0,255,0),new Color(0,0,255));

        Chart3DPanel chartPanel = new Chart3DPanel(chart);
        chartPanel.zoomToFit(new Dimension(640, 480));
        frame.setSize(640, 480);
        frame.setContentPane(chartPanel);
        frame.setVisible(true);


    }

    void drawHist(Frame model,double[] d,JFrame chartFrame){
        int limit = (int) d[0];
        double maxH = d[1];
        XYSeries series = new XYSeries("Гистограмма");
        XYSeries series2 = new XYSeries("Предел");
        double[] hist = model.getHist();
        int max = model.getMaxY();
        int min = model.getMinY();
        for (int j = min; j < max; j++) {
            series.add(j, hist[j]);
        }
        series2.add(limit, maxH);
        XYSeriesCollection collection = new XYSeriesCollection();
        collection.addSeries(series);
        collection.addSeries(series2);

        JFreeChart chart = ChartFactory.createHistogram(   // создание графика
                "Гистограмма",
                "b",
                "n",
                collection,
                PlotOrientation.VERTICAL,
                false,
                true,
                false
        );
        ChartPanel panel = new ChartPanel(chart);
        chartFrame.setSize(640, 480);
        chartFrame.setContentPane(panel);
        chartFrame.setVisible(true);
    }
}
