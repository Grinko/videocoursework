package system;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nixy on 19.10.2014.
 */
public class Correlator {

    List<EtalonImage> etalons;

    double[] result = new double[3];

    public Correlator(List<EtalonImage> etalons){
        this.etalons = etalons;
    }

    /**
     * Находит взаимную корреляцию по всему кадру
     * @param frame  Frame с одним кадром
     * @param index  номер эталона
     * @return
     */
    public List<double[]> find(Frame frame,int index, int padding){
        EtalonImage eimg = etalons.get(index);
        int w = frame.w;
        int h = frame.h;
        int w1 = eimg.getW();
        int h1 = eimg.getH();
        double em = eimg.mx;
        int ix = eimg.ix;
        int iy = eimg.iy;
        byte[][][] fData = frame.getData();
        byte[][][] eData = eimg.data;
        List<double[]> K = new ArrayList<double[]>();

        double mf = 0;
        for(int y1 = 0; y1 < h1; y1++){
            for (int x1 = 0; x1 < w1; x1++){
                mf += fData[0][iy+y1][ix+x1]& 0x0000ff;  // РАСЧЕТ МАТ ОЖИДАНИЯ ФРАГМЕНТА ИЗ КАДРА
            }
        }
        mf /= w1*h1;
        double k = 0;
        for(int y1 = 0; y1 < h1; y1++){
            for (int x1 = 0; x1 < w1; x1++){
                k += ((eData[0][y1][x1] & 0x0000ff )-em)*((fData[0][iy+y1][ix+x1]& 0x0000ff)-mf);
            }
        }
        K.add(new double[]{ix,iy,k});
        double eps = k*0.15;

        result[0] = ix; // координата х
        result[1] = iy; // координата у
        result[2] = k; // Максимальная корреляция

        int step = 8;

        int initY = iy - padding < 0 ? 0 : iy - padding;
        int initX = ix - padding < 0 ? 0 : ix - padding;
        int endY = iy + padding > h - h1 ? h - h1 : iy + padding;
        int endX = ix + padding > w - w1 ? w - w1 : ix + padding;
        initY = padding == 0 ? 0 : initY;
        initX = padding == 0 ? 0 : initX;
        endY = padding == 0 ? h - h1 : endY;
        endX = padding == 0 ? w - w1 : endX;

        for (int y = initY; y < endY ; y+=step){
            for (int x = initX; x < endX; x+=step){
                if (x != ix && y != iy) {
                    step = (Math.abs(x - ix) < w1 / 2 && Math.abs(y - iy) < h1 / 2) ? 2 : 8;
                    mf = 0;
                    for (int y1 = 0; y1 < h1; y1++) {
                        for (int x1 = 0; x1 < w1; x1++) {
                            mf += fData[0][y + y1][x + x1] & 0x0000ff;  // РАСЧЕТ МАТ ОЖИДАНИЯ ФРАГМЕНТА ИЗ КАДРА
                        }
                    }
                    mf /= w1 * h1;
                    k = 0;
                    for (int y1 = 0; y1 < h1; y1++) {
                        for (int x1 = 0; x1 < w1; x1++) {
                            k += ((eData[0][y1][x1] & 0x0000ff) - em) * ((fData[0][y + y1][x + x1] & 0x0000ff) - mf);
                        }
                    }
                    K.add(new double[]{x, y, k});

                    if (k > result[2] && Math.abs(k - result[2]) > eps) {
                        result[0] = x;
                        result[1] = y;
                        result[2] = k;
                    }
                }
            }
        }
        return K;
    }

    public void findField(Frame frame,int padding){
        for (int i = 0; i < etalons.size(); i++){
            find(frame, i,padding);
            etalons.get(i).getX().add((int)getResult()[0]);
            etalons.get(i).getY().add((int)getResult()[1]);
        }
    }

    /**
     * Находит норммальную взаимную корреляцию по всему кадру
     * @param frame  Frame с одним кадром
     * @param index  номер эталона
     * @return
     */
    public List<double[]> findNormal(Frame frame,int index){
        EtalonImage eimg = etalons.get(index);
        int w = frame.w;
        int h = frame.h;
        int w1 = eimg.getW();
        int h1 = eimg.getH();
        double em = eimg.mx;
        double ed = eimg.dx;
        int ix = eimg.ix;
        int iy = eimg.iy;
        byte[][][] fData = frame.getData();
        byte[][][] eData = eimg.data;
        List<double[]> K = new ArrayList<double[]>();

        result[0] = -1; // Максимальная корреляция
        result[1] = -1; // координата у
        result[2] = -1; // координата х

        int step = 8;

        for (int y = 0; y < h - h1 ; y+=step){
            for (int x = 0; x < w - w1; x+=step){
                step = (Math.abs(x - ix) < w1/2 && Math.abs(y - iy) < h1/2 )?  2 : 8;
                double mf = 0;
                for(int y1 = 0; y1 < h1; y1++){
                    for (int x1 = 0; x1 < w1; x1++){
                        mf += fData[0][y+y1][x+x1] & 0x0000ff;  // РАСЧЕТ МАТ ОЖИДАНИЯ ФРАГМЕНТА ИЗ КАДРА
                    }
                }
                mf /= w1*h1;
                if (mf == em)
                    em = mf;
                double df = 0;
                for(int y1 = 0; y1 < h1; y1++){
                    for (int x1 = 0; x1 < w1; x1++){
                        double a = ((fData[0][y+y1][x+x1]& 0x0000ff) - mf);
                        df += a*a;  // РАСЧЕТ Дисперсии  ФРАГМЕНТА ИЗ КАДРА
                    }
                }
                df /= w1*h1;
                double k = 0;
                for(int y1 = 0; y1 < h1; y1++){
                    for (int x1 = 0; x1 < w1; x1++){
                        k += ((eData[0][y1][x1]& 0x0000ff)-em)*((fData[0][y+y1][x+x1]& 0x0000ff)-mf);
                    }
                }
                k /= Math.sqrt(df*ed) *w1*h1;
                K.add(new double[]{x,y,k});

                if(k > result[2]){
                    result[0] = x;
                    result[1] = y;
                    result[2] = k;
                }
            }
        }
        return K;
    }

    public double[] getResult() {
        return result;
    }

    public int getEtalonCount(){
        return etalons.size();
    }


    public EtalonImage getEtalon(int index) {
        return etalons.get(index);
    }

    public List<EtalonImage> getEtalons(){
        return etalons;
    }


}
