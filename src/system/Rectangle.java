package system;

import java.awt.*;

/**
 * Author Grinch
 * Date: 04.11.2014
 * Time: 18:41
 */
public class Rectangle{
    private int x;
    private int y;
    private int h;
    private int w;
    private Color color;
    public Rectangle(int x,int y,int w,int h, Color color){
        this.x = x;
        this.y = y;
        this.w = w;
        this.h = h;
        this.color = color;
    }

    public Rectangle(Point p1, Point p2, int w1, int h1, Color color){
        this(p1.x * w1, p1.y * h1, p2.x * w1 - p1.x * w1 + w1, p2.y * h1 - p1.y * h1 + h1, color);
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getH() {
        return h;
    }

    public void setH(int h) {
        this.h = h;
    }

    public int getW() {
        return w;
    }

    public void setW(int w) {
        this.w = w;
    }

    public Color getColor() {
        return color;
    }
}
