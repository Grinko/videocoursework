package system;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nixy on 19.10.2014.
 */
public class EtalonImage {

    byte[][][] data;         // [цвет] [ кадр] [ высота] [ ширина]
    double mx;   // мат ожиданий
    double dx; // дисперсия
    int w;
    int h;
    int size;
    int ix;       // левый
    int iy;       // верхний угол
    int cx;       // центр эталона
    int cy;       // центр эталона

    List<Integer> x = new ArrayList<Integer>();
    List<Integer> y = new ArrayList<Integer>();

    Color color;



    public EtalonImage(byte[][][] data, int x,int y) {
        this.data = data;
        w = data[0][0].length;
        h = data[0].length;
        size = w*h;
        ix = x;
        iy = y;
        cx = x + w/2;
        cy = y + h/2;
        this.x.add(x);
        this.y.add(y);
        calcPrams();
    }

    void calcPrams(){
        for (int i = 0; i < h ; i++) {
            for (int j = 0; j < w; j++) {
                int r = data[0][i][j]  & 0x000000ff;
                mx += r;
            }
        }
        mx /= size;
        for (int i = 0; i < h ; i++) {
            for (int j = 0; j < w; j++) {
                int r = data[0][i][j]  & 0x000000ff;
                double a = r - mx;
                dx+=  a*a;
            }
        }
        dx/= size;
    }

    public List<Integer> getX() {
        return x;
    }

    public List<Integer> getY() {
        return y;
    }

    public int getIy() {
        return iy;
    }

    public int getIx() {
        return ix;
    }

    public int getW() {
        return w;
    }

    public int getH() {
        return h;
    }

    public int getPixel(int x,int y){
        if (x < w && y < h){
            return data[0][y][x]  & 0x000000ff;
        } else return 255;
    }

    public BufferedImage getSource(){
        BufferedImage source = new BufferedImage(w,h,BufferedImage.TYPE_INT_RGB);
        for (int i = 0; i < h ; i++) {
            for (int j = 0; j < w; j++) {
                int a = 0xff000000;
                int r = (data[0][i][j] << 16) & 0x00ff0000;
                int g = (data[1][i][j] << 8) & 0x0000ff00;
                int b = (data[2][i][j]) & 0x000000ff;
                int c = a + r + g + b;
                source.setRGB(j, i, c);
            }
        }
        return source;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }
}
