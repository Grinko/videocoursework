package system;

import java.awt.*;
import java.util.*;
import java.util.List;

/**
 * Author Grinch
 * Date: 04.11.2014
 * Time: 18:49
 */
public class ConnectedSpaceField {
    Random r = new Random();
    public static final double EPS = 360.;
    List<Rectangle> rect;
    Correlator cor;
    int colX;
    int colY;
    int h;
    int w1;
    int h1;
    int recoursePad = 1;
    int minimumEtalons = 3;
    double[][] field;
    int maximumEtalons = 9;

    public ConnectedSpaceField(Correlator cor, int colX, int colY,int h,int w1, int h1, int recoursePad, int minimumEtalons, int maximumEtalons){
        this.cor = cor;
        this.colX = colX;
        this.colY = colY;
        this.h = h;
        this.w1 = w1;
        this.h1 = h1;
        this.recoursePad = recoursePad;
        this.minimumEtalons = minimumEtalons;
        this.maximumEtalons = maximumEtalons;
        rect = new ArrayList<Rectangle>();
        calc();
    }

    public void makeNewField(Correlator cor, int colX, int colY, int h,int w1, int h1, int recoursePad, int minimumEtalons, int maximumEtalons) {
        this.cor = cor;
        this.colX = colX;
        this.colY = colY;
        this.h = h;
        this.w1 = w1;
        this.h1 = h1;
        this.recoursePad = recoursePad;
        this.minimumEtalons = minimumEtalons;
        this.maximumEtalons = maximumEtalons;
        rect = new ArrayList<Rectangle>();
        calc();
    }

    public void calc(){
        calcField();
        calcConnected();
    }

    private void calcConnected() {
        List<Point> points = new ArrayList<Point>();
        for (int x = 0; x < colX; x++){
            for (int y = 0; y < colY; y++){
                if (!Double.isNaN(field[x][y])){
                    points.clear();
                    connectedRecourse(x,y,field[x][y],points);
                    Collections.sort(points,new Comparator<Point>() {
                        @Override
                        public int compare(Point o1, Point o2) {
                            return (int) (o1.getX() - o2.getX());
                        }
                    });
                    int minx = points.get(0).x;
                    int maxx = points.get(points.size()-1).x;
                    Collections.sort(points,new Comparator<Point>() {
                        @Override
                        public int compare(Point o1, Point o2) {
                            return (int) (o1.getY() - o2.getY());
                        }
                    });
                    int miny = points.get(0).y;
                    int maxy = points.get(points.size()-1).y;
                    if (points.size() >= minimumEtalons && points.size() <= maximumEtalons){
                        rect.add(new Rectangle(new Point(minx,miny),new Point(maxx,maxy),w1,h1, new Color(r.nextInt(255),r.nextInt(255),r.nextInt(255))));
                    }
                }
            }
        }
    }

    private void connectedRecourse(int cX,int cY,double val,List<Point> points){
        Point p = new Point(cX,cY);
        points.add(p);
        field[cX][cY] = Double.NaN;
        for (int x = -recoursePad; x < 1+recoursePad; x++ ){
            for (int y = -recoursePad; y < 1+recoursePad; y++){
                if ((cX + x >= 0 && cX + x < colX)&&(cY + y >= 0 && cY + y < colY)){
                    if (Math.abs(val-field[cX+x][cY+y]) <= EPS){
                        connectedRecourse(cX+x,cY+y,val,points);
                    }
                }
            }
        }
    }



    private void calcField(){
        field = new double[colX][colY];
        for (int i = 0; i < colX*colY; i++){
            EtalonImage eImg = cor.getEtalon(i);
            List<Integer> x = eImg.getX();
            List<Integer> y = eImg.getY();

            int x0 = x.get(0);
            int y0 = h- y.get(0);
            int x1 = x.get(x.size()-1);
            int y1 = h-y.get(y.size()-1);
            int fx = i % colX;
            int fy = i / colX;
            if (Math.sqrt((x0-x1)*(x0-x1)+(y0-y1)*(y0-y1)) > 0){
                field[fx][fy] = Math.toDegrees(Math.atan2(y1-y0,x1-x0));
            }else{
                field[fx][fy] = Double.NaN;
            }
        }
    }


    public double[][] getField() {
        return field;
    }

    public List<Rectangle> getRect() {
        return rect;
    }
}
