package system;

import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

/**
 * Created by Nixy on 06.11.2014.
 */
public class PathControl {

    //private int pathRadius = 50;
    List<EtalonImage> cars = new ArrayList<EtalonImage>();
    HashMap<EtalonImage,Integer> flagsCars = new HashMap<EtalonImage, Integer>();
    HashMap<Path,Integer> flagsPaths = new HashMap<Path, Integer>();
    List<Path> paths = new ArrayList<Path>();

    public void addCars(List<EtalonImage> newCars, int pathRadius){
        if (cars.size() > 0){
            for (int i = 0 ; i < newCars.size(); i++){
                EtalonImage newCar = newCars.get(i);
                int x = newCar.cx;
                int y = newCar.cy;
                for(EtalonImage car : cars){

                    int carx = car.cx;
                    int cary = car.cy;

                    int dx = carx - x;
                    int dy = cary - y;

                    if (Math.sqrt(dx*dx + dy*dy) < pathRadius){
                        Path path;
                        if ( (path = findPathByCar(car)) != null){
                            path.add(newCar);
                            newCars.remove(newCar);
                            flagsCars.put(car, 1);
                            flagsPaths.put(path,1);
                            i--;
                        } else {
                            paths.add(new Path(car,newCar));
                            newCars.remove(newCar);
                            flagsCars.put(car, 1);
                            i--;
                        }
                        break;
                    }
                }
            }
            cars.addAll(newCars);

        }   else {
            cars.addAll(newCars);
        }
        clearOld();
    }

    private void clearOld() {
        for (int i = 0 ; i < cars.size(); i++){
            EtalonImage car = cars.get(i);
            if (flagsCars.containsKey(car)){
                flagsCars.put(car, flagsCars.get(car)+1);
                if (flagsCars.get(car) > 5){
                    cars.remove(car);
                    flagsCars.remove(car);
                    i--;
                }
            }  else {
                flagsCars.put(car,1);
            }
        }
        for (int i = 0 ; i < paths.size(); i++){
            Path path = paths.get(i);
            if (flagsPaths.containsKey(path)){
                flagsPaths.put(path,flagsPaths.get(path)+1);
                if (flagsPaths.get(path)>5){
                    paths.remove(path);
                    flagsPaths.remove(path);
                    i--;
                }
            } else {
                flagsPaths.put(path,1);
            }
        }

    }

    private Path findPathByCar(EtalonImage car) {
        for (Path path : paths){
            if (path.hasCar(car) != null){
                return path;
            }
        }
        return null;
    }

    public void drawPaths(Graphics2D graphics2D){
        for (Path path : paths){
            path.drawPath(graphics2D);
        }
    }


    public class Path {

        List<EtalonImage> cars = new ArrayList<EtalonImage>();
        Color c;

        public Path(EtalonImage car1,EtalonImage car2){
            cars.add(car1);
            cars.add(car2);
            Random r = new Random();
            c = new Color(r.nextInt(255),r.nextInt(255),r.nextInt(255));
        }

        public void add(EtalonImage car){
            cars.add(car);
        }

        public Path hasCar(EtalonImage car){
            return cars.indexOf(car) > -1 ? this : null;
        }

        public void drawPath(Graphics2D graphics2D){
            graphics2D.setColor(c);
            for (int i = 0 ; i < cars.size()-1; i++){
                EtalonImage start = cars.get(i);
                EtalonImage end = cars.get(i+1);
                int sx = start.cx;
                int sy = start.cy;
                int ex = end.cx;
                int ey = end.cy;
                graphics2D.drawLine(sx,sy,ex,ey);
            }
        }
    }


}
