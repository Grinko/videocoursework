package system;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by Nixy on 15.10.2014.
 */
public class Frame {

    byte[][][] data;         // [цвет] [ кадр] [ высота] [ ширина]

    Random r = new Random();

    PathControl control = new PathControl(); // память объектов

    double mx;  // мат ожиданий кадра
    int w = -1;   // ширина
    int h = -1;   // высота
    int minY;  // минимальный цвет кадра
    int maxY;   // максимальный цвет кадра
    int size = -1;  // количество пикселей w*h
    int frames = -1; // количество кадров
    int w1; //ширина эталона
    int h1; //высота эталона

    ConnectedSpaceField connectedSpaceField;
    int etalonsXCount;
    int etalonsYCount;

    Graphics2D graph; // канва

    List<Rectangle> rectangles = null;

    double hist[];

    final double  mask[][] = new double[][]{{1./9,1./9,1./9},
                                            {1./9,1./9,1./9},
                                            {1./9,1./9,1./9}};


    short[] noiseValues;
    {
        noiseValues = new short[5000000];
        for (int i = 0; i < noiseValues.length; i++){
           noiseValues[i] = (short) (this.r.nextGaussian()*48);
        }
    }
    public Frame(byte[][][] data) {
        this.data = data;
        w = data[0][0].length;
        h = data[0].length;
        size = w*h;

        hist = new double[256];
    }

    public void YCbCrToRGB(){

    for (int i = 0; i < h; i++) {
        for (int j = 0; j < w; j++) {

            double y =  (short)(data[0][i][j] & 255);
            double cb = (short)(data[1][i][j] & 255);
            double cr = (short)(data[2][i][j] & 255);

            double rd = y + 1.402*(cr - 128);
            double gd = y - 0.34414*(cb-128)-0.71414*(cr - 128);
            double bd = y + 1.772*(cb-128);

            if (rd < 0 ) rd = 0; if (rd > 255) rd = 255;
            if (gd < 0 ) gd = 0; if (gd > 255) gd = 255;
            if (bd < 0 ) bd = 0; if (bd > 255) bd = 255;

            data[0][i][j] = (byte)  rd; //r
            data[1][i][j] = (byte)  gd; //g
            data[2][i][j] = (byte)  bd;  //b

        }
    }

    }

    public void RGBtoGrayScale(){

       minY = 256;
       maxY = 0;
       mx = 0;
       for (int i = 0; i < h; i++)
           for (int j = 0; j < w; j++) {

               short r = (short)(data[0][i][j] & 255);
               short g = (short)(data[1][i][j] & 255);
               short b = (short)(data[2][i][j] & 255);
               short y = (short) ((r + g + b) / 3);


               if (y < minY) {
                   minY = y;
               }
               if (y > maxY) {
                   maxY = y;
               }

               mx += y;
               hist[y]++;

               data[2][i][j] = data[1][i][j] = data[0][i][j] = (byte) y;
           }
       mx/= size;


    }

    public void YCbCrToGrayScale(){

        for (int i = 0; i < h; i++) {
            for (int j = 0; j < w; j++) {
                short Y =  (short)((data[0][i][j])& 255);
                short Cb = (short)((data[1][i][j])& 255);
                short Cr = (short)((data[2][i][j])& 255);

                data[0][i][j] =(byte) Y;
                data[1][i][j] =(byte) Y;
                data[2][i][j] =(byte) Y;

            }
        }

    }

    public void YCbCrBin(double level){

        for (int i = 0; i < h; i++) {
            for (int j = 0; j < w; j++) {
                short r = (short)((data[0][i][j])& 255);
                short g = (short)((data[1][i][j])& 255);
                short b = (short)((data[2][i][j])& 255);

                if (r < level){
                    r  = g = b = 16;
                }else{
                    r = g = b = 235;
                }

                data[0][i][j] =(byte) r;
                data[1][i][j] =(byte) g;
                data[2][i][j] =(byte) b;

            }
        }

    }

    public void noise(){
        int noiseCount = 0;

        for (int i = 0; i < h; i++) {
            for (int j = 0; j < w; j++) {
                short r = (short)((data[0][i][j]  + noiseValues[this.r.nextInt(1000000)]));
                short g = (short)((data[1][i][j]  + noiseValues[this.r.nextInt(1000000)])& 255);
                short b = (short)((data[2][i][j]  + noiseValues[this.r.nextInt(1000000)])& 255);

                data[0][i][j] =(byte) r;
                data[1][i][j] =(byte) g;
                data[2][i][j] =(byte) b;

            }
        }


    }

    public void RGBToYCbCr(){

        for (int i = 0; i < h; i++) {
            for (int j = 0; j < w; j++) {
                short r = (short)(data[0][i][j] & 255);
                short g = (short)(data[1][i][j] & 255);
                short b = (short)(data[2][i][j] & 255);

                double er = r / 255.;
                double eg = g / 255.;
                double eb = b / 255.;

                double ey  = 0.299*er+0.587*eg+0.114*eb;
                double ecb =-0.169*er-0.331*eg+0.500*eb;
                double ecr = 0.500*er-0.419*eg-0.081*eb;

                double Y = 219*ey+16;
                double Cb = 224*ecb+128;
                double Cr = 224*ecr+128;

                if (Y  < 16) Y  = 16; if(Y  > 235) Y  = 235;
                if (Cb < 16) Cb = 16; if(Cb > 240) Cb = 240;
                if (Cr < 16) Cr = 16; if(Cr > 240) Cr = 240;
                data[0][i][j] =(byte) Y;//y;
                data[1][i][j] =(byte) Cb;//Cb;
                data[2][i][j] =(byte) Cr;//Cr;
            }
        }

    }

    public void bin(int limit){

         for (int i = 0; i < h; i++)
             for (int j = 0; j < w; j++) {
                 short r = (short)(data[0][i][j] & 255);
                 data[1][i][j] = data[2][i][j] =  data[0][i][j] = (byte) (r > limit ? 255 : 0);

             }

    }


    public double[] otcuBin(){
        double[] result = new double[2];

         int min = minY;
         int max = maxY;
         double mx = this.mx;
         int limit = 0;
         double maxH = 0;
         int a1 = 0;     // мат ожидание первого класса а1 = sum (0,i) (высота гист. * значение яркости) (hist[i]*i)
         int b1 = 0;     // кол-во пикселей первого класса 1 b1=sum (0,i) высот гист. (hist[i])
                         // мат ож 2 класса = мат ож изображения - мат ож 1 класса a2 = mx*size-a1
                         // кол во пикселей 2 класса = общее число пикселей - кол во пик 1 класса b2 = size - b1
                         // по методу Оцу необходимо найти максимум Db = w1*w2*(a1-a2)^2    w1, w2 вер-ть 1 и 2 классов
                         // w1 = b1/size   w2 = 1-w1
         double maxd = -1;
         for (int t = min; t < max; t++) {

             a1 += hist[t] * t;
             b1 += hist[t];
             double w1 = (double) b1 / size;
             double a = (double) a1 / b1 - (mx * size - (double) a1) / (size - b1);
             double Db = w1 * (1 - w1) * a * a;
             hist[t] /= size;  //нормировка

             if (Db > maxd) {
                 maxd = Db;
                 limit = t;
             }
             maxH = maxH < hist[t] ? hist[t] : maxH;
         }
         for (int i = 0; i < h; i++)
             for (int j = 0; j < w; j++) {
                 short r = (short)(data[0][i][j] & 255);
                 data[1][i][j] = data[2][i][j] =  data[0][i][j] = (byte) (r > limit ? 255 : 0);
             }
         result[0] = limit;  result[1]  = maxH;

        return result;
    }

    public void gaussianFilter(){
        reset();

        for (int i = 1; i < h - 1; i++)
            for (int j = 1; j < w - 1; j++) {
                int c = 0;
                for (int x = -1; x < 2; x++)
                    for (int y = -1; y < 2; y++) {
                        short r = (short)(data[0][i + y][j + x] & 255);
                        c +=  r * mask[1 + y][1 + x];
                    }
                data[1][i][j] = data[2][i][j] =  data[0][i][j] = (byte) c;
                mx += c;
                hist[c]++;
            }
        mx /= size;


    }



    public BufferedImage getSource(){
       // Controller.getMemoryUse("getSource()");

        BufferedImage source = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);

            //  Controller.getMemoryUse("Frame " + String.valueOf(f));
            for (int i = 0; i < h ; i++) {
                for (int j = 0; j < w; j++) {
                    int a = 0xff000000;
                    int r = (data[0][i][j] << 16) & 0x00ff0000;
                    int g = (data[1][i][j] << 8) & 0x0000ff00;
                    int b = (data[2][i][j]) & 0x000000ff;
                    int c = a + r + g + b;
                    source.setRGB(j, i, c);
                }
            }
        graph = source.createGraphics();
        if (rectangles != null){
            //drawGrid();
            for (Rectangle r : rectangles){
                graph.setColor(r.getColor());
                graph.draw(new java.awt.Rectangle(r.getX(), r.getY(), r.getW(), r.getH()));

            }
        }
        if (control.paths.size() > 0 ){
            control.drawPaths(graph);
        }

        return source;
    }

    public void drawGrid(){
        for (int i = 0; i < etalonsXCount; i++){
            for (int j = 0; j < etalonsYCount; j++){
                rectangles.add(0,new Rectangle(i*w1,j*h1,i*w1+w1,j*h1+h1,new Color(250,250,250)));
            }
        }
    }

    public List<EtalonImage> getEtalons(int row,int col){
        etalonsXCount = col;
        etalonsYCount = row;
        w1 = w/col;
        h1 = h/row;
        List<EtalonImage> etalons = new ArrayList<EtalonImage>();
        for(int i = 0 ; i < row; i++){                 // СТРОКИ ЭТАЛОНОВ
            for (int j = 0 ; j < col; j++) {           // СТОЛБЦЫ ЭТАЛОНОВ
                byte[][][] edata = new byte[3][h1][w1];
                for (int y = 0; y < h1; y++) {
                    System.arraycopy(data[0][y + h1 * i], 0 + w1 * j, edata[0][y], 0, w1);
                    System.arraycopy(data[1][y + h1 * i], 0 + w1 * j, edata[1][y], 0, w1);
                    System.arraycopy(data[2][y + h1 * i], 0 + w1 * j, edata[2][y], 0, w1);
                }
                etalons.add(new EtalonImage(edata, 0 + w1 * j, 0 + h1 * i));
            }
        }
        return etalons;
    }

    public List<EtalonImage> getEtalons(){
        List<EtalonImage> etalons = new ArrayList<EtalonImage>();
        for (Rectangle rectangle: rectangles){
            int w1 = rectangle.getW();
            int h1 = rectangle.getH();
            int inity = rectangle.getY();
            int initx = rectangle.getX();
            byte[][][] edata = new byte[3][h1][w1];
            for (int y = 0; y < h1; y++) {
                System.arraycopy(data[0][y + inity ], initx , edata[0][y], 0, w1);
                System.arraycopy(data[1][y + inity ], initx , edata[1][y], 0, w1);
                System.arraycopy(data[2][y + inity ], initx , edata[2][y], 0, w1);
            }
            etalons.add(new EtalonImage(edata, initx, inity));
        }
        return etalons;
    }

    public void doConnectedSpaces(Correlator correlator, int recoursePad, int minimumEtalons, int maximumEtalons){
        if (connectedSpaceField == null){
            connectedSpaceField = new ConnectedSpaceField(correlator,etalonsXCount,etalonsYCount,h,w1,h1,recoursePad,minimumEtalons,maximumEtalons);
        }else{
            connectedSpaceField.makeNewField(correlator,etalonsXCount,etalonsYCount,h,w1,h1,recoursePad,minimumEtalons,maximumEtalons);
        }
        rectangles = connectedSpaceField.getRect();

    }



    public byte[][][] getData() {
        return data;
    }

    public void reset(){
        for (int i = 0 ; i < frames; i++) {
            mx = 0;
            for (int j = 0 ; j < 256; j++)
                hist[j] = 0;
        }
    }

    public void setData(byte[][][] data) {
        this.data = data;
    }

    public double[] getHist() {
        return hist;
    }

    public int getMinY() {
        return minY;
    }

    public int getMaxY() {
        return maxY;
    }

    public int getH() {
        return h;
    }

    public ConnectedSpaceField getConnectedSpaceField() {
        return connectedSpaceField;
    }

    public int getW() {
        return w;
    }

    public PathControl getControl() {
        return control;
    }

    public void setControl(PathControl control) {
        this.control = control;
    }

    public void doCars(int pathRadius) {
        control.addCars(getEtalons(),pathRadius);
    }
}
